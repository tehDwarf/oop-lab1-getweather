package core;

import core.winfo.NullWeatherInfo;
import core.winfo.WeatherInfo;
import core.winfo.iWeatherInfo;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Date;

public class WeatherTest {

    private String EXISTING_CITY = "London";
    private String UNEXISTING_CITY = "SOME_UNEXISTING_CITY";

    @Test(description = "positive")
    public void testOpenWeatherGetCurrentWeather() throws Exception{
        testGetCurrentWeather(new OpenWeatherAPI(), EXISTING_CITY, WeatherInfo.class);
    }

    @Test(description = "positive")
    public void testWeatherBitGetCurrentWeather() throws Exception{
        testGetCurrentWeather(new WeatherBitAPI(), EXISTING_CITY, WeatherInfo.class);
    }

    @Test(description = "negative")
    public void negativeTestOpenWeatherGetCurrentWeather() throws Exception{
        testGetCurrentWeather(new OpenWeatherAPI(), UNEXISTING_CITY, NullWeatherInfo.class);
    }

    @Test(description = "negative")
    public void negativeTestWeatherBitGetCurrentWeather() throws Exception{
        testGetCurrentWeather(new WeatherBitAPI(), UNEXISTING_CITY, NullWeatherInfo.class);
    }

    private void testGetCurrentWeather(WeatherAPI w, String city, Class<?> expectedObject) throws Exception {
        Assert.assertNotNull(city);
        iWeatherInfo info = w.getCurrentWeather(city);

        Assert.assertNotNull(info);
        Assert.assertTrue(info.getClass().isAssignableFrom(expectedObject));

        if (info instanceof WeatherInfo){
            long diff = new Date().getTime() - ((WeatherInfo) info).getDatetime().getTime();
            Assert.assertTrue(diff > 3600*24, "Too much difference between current time and " +
                    "the forecasting time. Something definitely went wrong");
        }

        System.out.println(info.getPrettyString());
    }
}


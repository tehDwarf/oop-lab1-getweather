package core;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import core.winfo.iWeatherInfo;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.net.URI;

public abstract class WeatherAPI {

    public final static String DEFAULT_CITY = "Chisinau";

    protected Gson gson = new GsonBuilder().setPrettyPrinting().create();
    protected HttpClient http = HttpClients.createDefault();

    /**
     * Uses {@link #DEFAULT_CITY} as a location
     */
    public iWeatherInfo getCurrentWeather() throws IOException {
        return getCurrentWeather(WeatherAPI.DEFAULT_CITY);
    }

    /**
     * Get current weather for specified cityName. If cityName == null, then silently assumes {@link #DEFAULT_CITY}
     * 
     * @return WeatherInfo object on success, null on failure.
     *         The most common example of failure would be "unrecognized city"
     * @throws IOException when unable to connect API service for some reason
     */
    public abstract iWeatherInfo getCurrentWeather(String cityName) throws IOException;

    /**
     * Sends GET request to specified URI and returns contents as String
     *
     * @param uri URI to send request
     * @param expectedCode code expected on successful API call
     * @return String representation of received response contents
     * @throws IOException if something went wrong with sending request
     */
    protected final String execGetForContents(URI uri, int expectedCode) throws IOException {
        String strResponse = "";

        HttpGet get = new HttpGet(uri);
        HttpResponse httpResponse = http.execute(get);
        HttpEntity entity = httpResponse.getEntity();
        if (entity != null){
            strResponse = IOUtils.toString(entity.getContent());
        }
        if (httpResponse.getStatusLine().getStatusCode() != expectedCode){
            throw new RuntimeException(
                    String.format("%s: %s", httpResponse.getStatusLine().toString(), strResponse));
        }
        return strResponse;
    }

}

package core;

import core.winfo.NullWeatherInfo;
import core.winfo.WeatherInfo;
import core.winfo.WeatherInfoBuilder;
import core.winfo.iWeatherInfo;
import org.apache.http.client.utils.URIBuilder;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WeatherBitAPI extends WeatherAPI {
    private final String API_KEY = "6267ec33eb60494391d6efdff3896475";
    private final String API_LINK = "http://api.weatherbit.io/v2.0/current/";

    @Override
    public iWeatherInfo getCurrentWeather(String cityName) throws IOException {
        WeatherInfo info = null;

        URIBuilder uri = new URIBuilder()
                .setPath(API_LINK)
                .addParameter("city", cityName)
                .addParameter("key", API_KEY);
        try {
            String jsonResponse = super.execGetForContents(uri.build(), 200);
            ResponseCurrentWeather parsed = gson.fromJson(jsonResponse, ResponseCurrentWeather.class);
            info = new WeatherInfoBuilder(parsed.getData().getCityName(), parsed.getData().getTemp())
                    .setTemperature(parsed.getData().getTemp())
                    .setWeatherDescription(parsed.getData().getWeather().getDescription())
                    .setAsOfDate(parsed.getData().getDatetime())
                    .build();
        } catch (RuntimeException e){
            return new NullWeatherInfo(cityName, e.getLocalizedMessage());
        } catch (URISyntaxException e) {
            // not supposed to happen.
        }

        return info;
    }

    private class ResponseCurrentWeather {
        private int count;
        private Data[] data; // ignoring all the rest

        public int getCount() {
            return count;
        }

        public Data getData() {
            return data[0];
        }

        class Data {
            private Weather weather;
            private String country_code;
            private String city_name;
            private String sunrise;
            private String sunset;
            private double temp;
            private String wind_spd;
            private String wind_cdir_full;
            private String ob_time;

            public Date getDatetime(){
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    return formatter.parse(ob_time);
                } catch (ParseException e) {
                    return null;
                }
            }

            public Weather getWeather() {
                return weather;
            }

            public String getCountry_code() {
                return country_code;
            }

            public String getCityName() {
                return city_name;
            }

            public String getSunrise() {
                return sunrise;
            }

            public String getSunset() {
                return sunset;
            }

            public double getTemp() {
                return temp;
            }

            public String getWind_spd() {
                return wind_spd;
            }

            public String getWind_cdir_full() {
                return wind_cdir_full;
            }
        }

        class Weather {
            private String description;

            public String getDescription() {
                return description;
            }
        }
    }
}

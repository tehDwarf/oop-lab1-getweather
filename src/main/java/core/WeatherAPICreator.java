package core;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Singleton, Factory Method
 */
public class WeatherAPICreator {

    public enum SupportedAPIs {
        WEATHERBIT,
        OPENWEATHER
    }

    private final static Map<SupportedAPIs, WeatherAPI> APIs = new HashMap<>();
    private final static SupportedAPIs DEFAULT_API = SupportedAPIs.OPENWEATHER;

    private static WeatherAPICreator myInstance;

    static {
        APIs.put(SupportedAPIs.WEATHERBIT, new WeatherBitAPI());
        APIs.put(SupportedAPIs.OPENWEATHER, new OpenWeatherAPI());
    }

    private WeatherAPICreator() {}

    public static WeatherAPICreator getInstance() {
        if (myInstance == null) {
            myInstance = new WeatherAPICreator();
        }

        return myInstance;
    }

    /**
     * @param name - API name (case-insensitive)
     * @return returns corresponding API instance if found, null otherwise
     */
    public WeatherAPI getAPIByAlias(String name) {
        if (name == null) {
            throw new IllegalArgumentException("API name should not be null");
        }

        return APIs.get(SupportedAPIs.valueOf(name.toUpperCase()));
    }

    public WeatherAPI getDefaultAPI(){
        return APIs.get(DEFAULT_API);
    }

    public static Set<String> getSupportedAPIs() {
        Set<SupportedAPIs> keys = APIs.keySet();
        return keys.stream().map(Enum::toString).collect(Collectors.toSet());
    }
}

package core.winfo;

import com.sun.istack.internal.Nullable;

import java.text.SimpleDateFormat;
import java.util.Date;

public class WeatherInfo implements iWeatherInfo {
    String cityName;
    double temperature;
    String description;
    Date datetime;
    String sunrise;
    String sunset;

    private SimpleDateFormat format = new SimpleDateFormat("EEEE, d MMM yyyy HH:mm");

    WeatherInfo() { }

    /*
     * Format: {} - obligatory, [] - optional
     *
     * { `cityName` }, [ `asOfDate` ]
     * [ `weatherDescription` ,] { `temperature` }{ unit }
     *
     * [ sunrise: `sunrise`
     *   sunset:  `sunset` ]
     */
    @Override
    public String getPrettyString(){
        StringBuilder out = new StringBuilder();

        out.append(cityName);
        if (datetime != null){
            out.append(", ").append(format.format(datetime)).append(getDatetimeOverdueAsString());
        }
        out.append("\r\n");
        out.append(description).append(", ").append(temperature).append('\u2103').append("\r\n");

        if (sunrise != null && sunset != null) {
            out.append("\r\n\t\n")
                    .append("Sunrise: ").append(sunrise)
                    .append("Sunset:  ").append(sunset);
        }

        return out.toString();
    }

    private String getDatetimeOverdueAsString(){
        long overdueInHours = (new Date().getTime() - datetime.getTime()) / 3600000;
        long overdueInMins = (new Date().getTime() - datetime.getTime()) / 60000;
        if (overdueInHours > 6){
            return " (>6h overdue)";
        }
        else if (overdueInHours > 1 ){
            return String.format(" (~%1dh overdue)", overdueInHours);
        }
        else if (overdueInMins <= 10){
            return "";
        }
        else
            return String.format(" (~%2d minutes overdue)", overdueInMins);
    }

    public String getCityName() {
        return cityName;
    }

    public double getTemperature() {
        return temperature;
    }

    public String getDescription() {
        return description;
    }

    public String getSunrise() {
        return sunrise;
    }

    public String getSunset() {
        return sunset;
    }

    public Date getDatetime() {
        return datetime;
    }

}

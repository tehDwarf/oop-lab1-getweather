package core.winfo;

import core.winfo.iWeatherInfo;

public class NullWeatherInfo implements iWeatherInfo {
    private String cityName;
    private String errorMsg;

    public NullWeatherInfo(String cityName, String errorMsg){
        this.errorMsg = errorMsg;
        this.cityName = cityName;
    }

    @Override
    public String getPrettyString() {
        return String.format("Unable to fetch current weather for city %s, " +
                "server answered with an error: %s", cityName, errorMsg);
    }
}

package core.winfo;

import java.util.Date;


public class WeatherInfoBuilder {

    private WeatherInfo instance;

    public WeatherInfoBuilder(String cityName, double temperature) {
        this.instance = new WeatherInfo(    );
        this.instance.temperature = temperature;
        this.instance.cityName = cityName;
    }

    public WeatherInfoBuilder setCityName(String cityName) {
        this.instance.cityName = cityName;
        return this;
    }

    public WeatherInfoBuilder setTemperature(double temperature) {
        this.instance.temperature = temperature;
        return this;
    }

    public WeatherInfoBuilder setWeatherDescription(String desc) {
        this.instance.description = desc;
        return this;
    }

    public WeatherInfoBuilder setAsOfDate(Date date) {
        this.instance.datetime = date;
        return this;
    }

    public WeatherInfoBuilder setSunriseTime(String hhmm) {
        this.instance.sunrise = hhmm;
        return this;
    }

    public WeatherInfoBuilder setSunsetTime(String hhmm) {
        this.instance.sunset = hhmm;
        return this;
    }

    public WeatherInfo build(){
        return this.instance;
    }

}

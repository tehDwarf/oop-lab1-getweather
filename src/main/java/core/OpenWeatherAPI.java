package core;

import core.winfo.NullWeatherInfo;
import core.winfo.WeatherInfo;
import core.winfo.WeatherInfoBuilder;
import core.winfo.iWeatherInfo;
import org.apache.http.client.utils.URIBuilder;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;

public class OpenWeatherAPI extends WeatherAPI {
    private final String API_KEY = "cb0124277dcd64efb1196d441adaa42e";
    private final String API_LINK = "http://api.openweathermap.org/data/2.5/weather";

    @Override
    public iWeatherInfo getCurrentWeather(String cityName) throws IOException {
        WeatherInfo answer = null;

        URIBuilder uri = new URIBuilder()
                .setPath(API_LINK)
                .addParameter("q", cityName)
                .addParameter("units", "metric")
                .addParameter("APPID", API_KEY);
        try {
            String jsonResponse = super.execGetForContents(uri.build(), 200);
            ResponseCurrentWeather parsed = gson.fromJson(jsonResponse, ResponseCurrentWeather.class);
            answer = new WeatherInfoBuilder(parsed.getName(), parsed.getMain().getTemp())
                    .setTemperature(parsed.getMain().getTemp())
                    .setWeatherDescription(parsed.getWeather().getDescription())
                    .setAsOfDate(parsed.getDt())
                    .build();
        } catch (RuntimeException e) {
            return new NullWeatherInfo(cityName, e.getLocalizedMessage());
        } catch (URISyntaxException e) {
            // not supposed to happen, so simply do nothing
        }

        return answer;
    }

    private class ResponseCurrentWeather {
        private String name;
        private int id;
        private int cod;
        private long dt;

        private Main main;
        private Weather[] weather;

        private ResponseCurrentWeather() {
        }

        public Date getDt(){
            //dt assumed to store UTC time in SECONDS
            // so multiply by 1000 to get millis
            return new Date(dt*1000);
        }

        public int getId() {
            return id;
        }

        public int getCod() {
            return cod;
        }

        Main getMain() {
            return main;
        }

        String getName() {
            return name;
        }

        Weather getWeather() {
            return weather[0];
        }

        class Main {

            private double temp;
            private double temp_min;
            private double temp_max;

            public double getTemp() {
                return temp;
            }

            public double getTemp_min() {
                return temp_min;
            }

            public double getTemp_max() {
                return temp_max;
            }
        }

        class Weather {
            private String description;

            public String getDescription() {
                return description;
            }
        }

    }
}

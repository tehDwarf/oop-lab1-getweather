import core.WeatherAPI;
import core.WeatherAPICreator;
import org.apache.commons.cli.*;
import core.winfo.iWeatherInfo;

import java.io.IOException;


public class App {

    public static final String APP_NAME = "get-weather";

    public static void main(String[] args) {
        Options opts = initCLIOptions();
        CommandLineParser parser = new DefaultParser();
        HelpFormatter help = new HelpFormatter();

        WeatherAPICreator apiCreator = WeatherAPICreator.getInstance();
        WeatherAPI weatherAPI = apiCreator.getDefaultAPI();
        String cityName = WeatherAPI.DEFAULT_CITY;

        try {
            CommandLine cl = parser.parse(opts, args);

            if (cl.hasOption("source")){
                weatherAPI = apiCreator.getAPIByAlias(cl.getOptionValue("source"));
                if (weatherAPI == null){
                    System.out.printf("Unsupported API alias: <%s> \r\n\r\n", cl.getOptionValue("source"));
                    help.printHelp(APP_NAME, opts);
                    System.exit(0);
                }
            }
            if (cl.hasOption("city")){
                cityName = cl.getOptionValue("city");
            }

            iWeatherInfo i = weatherAPI.getCurrentWeather(cityName);
            String answer = i.getPrettyString();
            System.out.println(answer);

        } catch (IOException e) {
            System.out.println("Something went wrong while retrieving the data from API service. \r\n" +
                    "Kindly check your internet connection or API_KEY validity");
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage() + "\r\n");
            help.printHelp(APP_NAME, opts);
        }
    }

    private static Options initCLIOptions(){
        Option city = Option.builder()
                .argName("city")
                .hasArg()
                .desc("desired city")
                .longOpt("city")
                .build();
        Option source = Option.builder()
                .argName("api service")
                .hasArg()
                .desc("alias of the API service the weather info will be pulled from. \n" +
                        "supported aliases: " + String.join(", ", WeatherAPICreator.getSupportedAPIs()))
                .longOpt("source")
                .optionalArg(true)
                .build();

        return new Options()
                .addOption(city)
                .addOption(source);
    }

}

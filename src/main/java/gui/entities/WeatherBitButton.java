package gui.entities;

import javax.swing.*;

public class WeatherBitButton extends JButton {

    public WeatherBitButton() {
    }

    public WeatherBitButton(String text) {
        super(text);
    }
}

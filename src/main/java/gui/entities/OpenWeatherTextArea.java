package gui.entities;

import javax.swing.*;
import java.awt.*;

public class OpenWeatherTextArea extends JTextArea {
    public OpenWeatherTextArea() {
        this.setForeground(Color.RED);
        this.setBackground(Color.lightGray);
    }
}

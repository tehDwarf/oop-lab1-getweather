package gui.entities;

import javax.swing.*;
import java.awt.*;

public class OpenWeatherButton extends JButton {

    public OpenWeatherButton() {}

    public OpenWeatherButton(String text) {
        super(text);
    }

    private static final long serialVersionUID = 1L;

    private Color circleColor = Color.BLACK;

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Dimension originalSize = super.getPreferredSize();
        int gap = (int) (originalSize.height * 0.2);
        int x = originalSize.width + gap;
        int y = gap;
        int diameter = originalSize.height - (gap * 2);

        g.setColor(circleColor);
        g.fillOval(x, y, diameter, diameter);
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension size = super.getPreferredSize();
        size.width += size.height;
        return size;
    }
}

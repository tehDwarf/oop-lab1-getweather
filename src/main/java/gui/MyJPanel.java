package gui;

import core.WeatherAPI;
import core.WeatherAPICreator;
import core.winfo.iWeatherInfo;
import gui.factory.AbstractViewFactory;
import gui.prototype.JPanelPrototype;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class MyJPanel extends JPanel implements JPanelPrototype{
    private JTextField inputBox;
    private JTextArea outputBox;
    private JButton sendRequestBtn;
    private String alias;

    private void init() {
        String titleStringPattern = "<html><h1><strong><i>%s</i></strong></h1><hr></html>";
        Dimension jpSize = new Dimension(600, 800);
        Dimension obSize = new Dimension(400, 100);
        int ibWidth = 80;

        Dimension oldsize = this.inputBox.getPreferredSize();
        oldsize.width = ibWidth;
        this.inputBox.setPreferredSize(oldsize);

        this.outputBox.setEditable(false);
        this.outputBox.setPreferredSize(obSize);

        sendRequestBtn.addActionListener(new APIRequestBtnListener());

        this.setBorder(new EmptyBorder(10, 10, 10, 10));
        this.setLayout(new GridBagLayout());
        this.setPreferredSize(jpSize);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.anchor = GridBagConstraints.NORTH;
        this.add(new JLabel(String.format(titleStringPattern, this.alias)), gbc);

        gbc.anchor = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(10, 10, 10, 10);

        JPanel buttons = new JPanel(new GridBagLayout());
        buttons.add(inputBox, gbc);
        buttons.add(sendRequestBtn, gbc);
        buttons.add(outputBox, gbc);

        gbc.weighty = 1;
        this.add(buttons, gbc);
    }

    private MyJPanel(){}

    // factory approach
    public MyJPanel(AbstractViewFactory factory) {
        this.inputBox = factory.createTextBox();
        this.outputBox = factory.createTextArea();
        this.sendRequestBtn = factory.createButton("OK");
        this.alias = factory.getAlias().toString();
        init();
    }

    // prototype approach
    public MyJPanel(JTextArea outputBox, JTextField inputBox, JButton btn, String title) {
        this.inputBox = inputBox;
        this.outputBox = outputBox;
        this.sendRequestBtn = btn;
        this.alias = title;
    }

    public void showMe(String frameTitle) {
        JFrame frame = new JFrame(frameTitle);
        frame.setContentPane(this);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setResizable(false);
        frame.pack();
        frame.setLocationRelativeTo(MainGUIForm.rootPanel);
        frame.setVisible(true);
    }

    @Override
    public MyJPanel copy() {
        try {
            // not quite "clear" implementation (i`d say at all :]), but the reverse is not in scope.
            MyJPanel copy = new MyJPanel();
            copy.sendRequestBtn = this.sendRequestBtn.getClass().getConstructor(String.class).newInstance("OK");
            copy.inputBox = this.inputBox.getClass().getConstructor().newInstance();
            copy.outputBox = this.outputBox.getClass().getConstructor().newInstance();
            copy.alias = this.alias;
            copy.init();
            return copy;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private class APIRequestBtnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            SwingUtilities.invokeLater(() -> {
                String city = StringUtils.trimToNull(inputBox.getText());
                if (city != null ) {
                    WeatherAPI api = WeatherAPICreator.getInstance().getAPIByAlias(alias);
                    try {
                        iWeatherInfo wi = api.getCurrentWeather(city);
                        outputBox.setText(wi.getPrettyString());
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            });
        }
    }
}

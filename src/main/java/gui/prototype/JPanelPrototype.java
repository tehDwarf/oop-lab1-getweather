package gui.prototype;

import gui.MyJPanel;

import javax.swing.*;

public interface JPanelPrototype {
    MyJPanel copy();
}

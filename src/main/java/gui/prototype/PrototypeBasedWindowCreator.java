package gui.prototype;

import core.WeatherAPICreator;
import gui.MyJPanel;
import gui.entities.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import static core.WeatherAPICreator.SupportedAPIs;

public class PrototypeBasedWindowCreator implements ActionListener {

    private static Hashtable<SupportedAPIs, MyJPanel> map = new Hashtable<>();
    static {
        // prototypes
        map.put(SupportedAPIs.WEATHERBIT, new MyJPanel(
                        new WeatherBitTextArea(),
                        new WeatherBitTextField(),
                        new WeatherBitButton("OK"),
                        SupportedAPIs.WEATHERBIT.toString())
        );

        map.put(SupportedAPIs.OPENWEATHER, new MyJPanel(
                        new OpenWeatherTextArea(),
                        new OpenWeatherTextField(),
                        new OpenWeatherButton("OK"),
                        SupportedAPIs.OPENWEATHER.toString())
        );
    }
    private MyJPanel panel;

    public PrototypeBasedWindowCreator(WeatherAPICreator.SupportedAPIs api) {
        this.panel = map.get(api).copy();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        panel.showMe("IPP Weatherapp");
    }
}

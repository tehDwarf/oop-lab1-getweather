package gui.factory;

import core.WeatherAPICreator;
import gui.MyJPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FactoryBasedWindowCreator implements ActionListener {

    private AbstractViewFactory factory;

    public FactoryBasedWindowCreator(WeatherAPICreator.SupportedAPIs api) {
        switch (api) {
            case OPENWEATHER:
                this.factory = new OpenWeatherViewFactory();
                break;
            case WEATHERBIT:
                this.factory = new WeatherBitViewFactory();
                break;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        MyJPanel jPanel = new MyJPanel(factory);
        jPanel.showMe("IPP Weatherapp");
    }

}


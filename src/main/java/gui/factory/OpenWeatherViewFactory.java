package gui.factory;

import core.WeatherAPICreator;
import gui.entities.OpenWeatherButton;
import gui.entities.OpenWeatherTextArea;
import gui.entities.OpenWeatherTextField;

import javax.swing.*;

public class OpenWeatherViewFactory extends AbstractViewFactory {

    @Override
    public JButton createButton(String text) {
        return new OpenWeatherButton(text);
    }

    @Override
    public JTextField createTextBox() {
        return new OpenWeatherTextField();
    }

    @Override
    public JTextArea createTextArea() {
        return new OpenWeatherTextArea();
    }

    @Override
    public WeatherAPICreator.SupportedAPIs getAlias() {
        return WeatherAPICreator.SupportedAPIs.OPENWEATHER;
    }


}

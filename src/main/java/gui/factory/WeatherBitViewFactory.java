package gui.factory;

import core.WeatherAPICreator;
import gui.entities.OpenWeatherTextArea;
import gui.entities.WeatherBitButton;
import gui.entities.WeatherBitTextArea;
import gui.entities.WeatherBitTextField;

import javax.swing.*;
import java.awt.*;

public class WeatherBitViewFactory extends AbstractViewFactory {

    @Override
    public JButton createButton(String text) {
        return new WeatherBitButton(text);
    }

    @Override
    public JTextField createTextBox() {
        return new WeatherBitTextField();
    }

    @Override
    public JTextArea createTextArea() {
        return new WeatherBitTextArea();
    }

    @Override
    public WeatherAPICreator.SupportedAPIs getAlias() {
        return WeatherAPICreator.SupportedAPIs.WEATHERBIT;
    }
}

package gui.factory;

import core.WeatherAPICreator;

import javax.swing.*;

public abstract class AbstractViewFactory {

    public abstract JButton createButton(String text);

    public abstract JTextField createTextBox();

    public abstract JTextArea createTextArea();

    public abstract WeatherAPICreator.SupportedAPIs getAlias();
}

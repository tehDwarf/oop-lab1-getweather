package gui;

import gui.factory.FactoryBasedWindowCreator;
import gui.prototype.PrototypeBasedWindowCreator;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

import static core.WeatherAPICreator.SupportedAPIs;

@SuppressWarnings("ALL")
public class MainGUIForm {
    public final static JPanel rootPanel = new JPanel();
    private JButton weatherbit = new JButton("Weatherbit service");
    private JButton openweather = new JButton("OpenWeather service");

    private MainGUIForm(){
        //weatherbit .addActionListener(new FactoryBasedWindowCreator(SupportedAPIs.WEATHERBIT));
        //openweather.addActionListener(new FactoryBasedWindowCreator(SupportedAPIs.OPENWEATHER));

        weatherbit .addActionListener(new PrototypeBasedWindowCreator(SupportedAPIs.WEATHERBIT));
        openweather.addActionListener(new PrototypeBasedWindowCreator(SupportedAPIs.OPENWEATHER));

        rootPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        rootPanel.setLayout(new GridBagLayout());

        rootPanel.setPreferredSize(new Dimension(350, 400));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.anchor = GridBagConstraints.NORTH;

        rootPanel.add(new JLabel("<html><h1><strong><i>Weather App</i></strong></h1><hr></html>"), gbc);

        gbc.anchor = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(10, 10, 10, 10);

        JPanel buttons = new JPanel(new GridBagLayout());
        buttons.add(openweather, gbc);
        buttons.add(weatherbit, gbc);

        gbc.weighty = 1;
        rootPanel.add(buttons, gbc);
    }

    public static void main(String[] args) {

        JFrame frame = new JFrame("IPP Weatherapp");
        MainGUIForm clientForm = new MainGUIForm();
        frame.setContentPane(clientForm.rootPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }
}
